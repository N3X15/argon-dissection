# Payload 22-12-2017

This payload was discovered prior to December 22nd, 2017, and was the first payload of Argon detected.

At this point, the commit hash of the payload was not recorded, so meta.yml is not present.
Some metadata will therefore be missing.

## Metadata

<table>
<tr><th>Revision:</th><td>N/A</td></tr>
<tr><th>Commit:</th><td>N/A</td></tr>
<tr><th>Last-Modified:</th><td>N/A</td></tr>
<tr><th>Hostname:</th><td>N/A</td></tr>
</table>
