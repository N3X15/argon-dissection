**HELLO YES I AM MOVING EVERYTHING, BE CALM**

# Argon

This is some malicious ad-hosting stuff that popped up on 4chan recently (as of 12/20/2017), calling itself "Argon stats" (sic).  It manifests as a script embedded in the site (XPath /html/head/script[6]), meaning it cannot be blocked without blocking 4chan itself.  Users have claimed malicious behavior, drawing my interest, as I used to dissect worms and virii for fun when I was a kid.

I have analyzed and annotated the script for shits and giggles.  This information is provided as an educational exercise.

The owner of this software is currently unknown and may very well never be known.  Google searches for "Argon stats" and some of the headers seen in this software have been fruitless.

# WARNING

Users have claimed that this software downloads and installs malicious software, and redirects.

Now, given that these claims are from 4chan, they are to be taken with a grain of salt, but at least one malicious payload has been found.

**DO NOT RUN THIS OUTSIDE OF A VIRTUAL MACHINE YOU CAN STAND TO LOSE.**

# Contents

<table>
<tr><th>loader/</th><td>The embedded script that loads other things.</td></tr>
<tr><th>payloads/</th><td>Discovered payloads.  These are partially encrypted, and decrypted by the loader.</td></tr>
<tr><th>traffic/</th><td>Recorded traffic.</td></tr>
</table>

# Hostnames Contacted

All hostnames listed are in a load-balancing arrangement.  Subdomains appear to be somewhat random.

## amgload.net

### DiG
```
; <<>> DiG 9.10.3-P4-Ubuntu <<>> amgload.net
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 58841
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;amgload.net.                   IN      A

;; ANSWER SECTION:
amgload.net.            299     IN      A       127.0.0.1

;; Query time: 164 msec
;; SERVER: 127.0.1.1#53(127.0.1.1)
;; WHEN: Wed Dec 20 03:45:55 PST 2017
;; MSG SIZE  rcvd: 56
```
### WHOIS
```
   Domain Name: AMGLOAD.NET
   Registry Domain ID: 2195218237_DOMAIN_NET-VRSN
   Registrar WHOIS Server: whois.name.com
   Registrar URL: http://www.name.com
   Updated Date: 2017-12-04T11:45:44Z
   Creation Date: 2017-12-03T13:34:07Z
   Registry Expiry Date: 2019-12-03T13:34:07Z
   Registrar: Name.com, Inc.
   Registrar IANA ID: 625
   Registrar Abuse Contact Email: abuse@name.com
   Registrar Abuse Contact Phone: 7202492374
   Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
   Name Server: NS1.AMGLOAD.NET
   Name Server: NS2.AMGLOAD.NET
   DNSSEC: unsigned
   URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of whois database: 2017-12-20T11:38:11Z <<<

For more information on Whois status codes, please visit https://icann.org/epp
```

## piguiqproxy.com
Appears to be primary asset delivery node.

### DiG
```
; <<>> DiG 9.10.3-P4-Ubuntu <<>> PIGUIQPROXY.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56936
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;PIGUIQPROXY.com.               IN      A

;; ANSWER SECTION:
PIGUIQPROXY.com.        1819    IN      A       185.187.80.173
PIGUIQPROXY.com.        1819    IN      A       185.187.80.171
```

### WHOIS
```
Domain Name: PIGUIQPROXY.COM
Registry Domain ID: 2037813135_DOMAIN_COM-VRSN
Registrar WHOIS Server: whois.imena.ua
Registrar URL: http://www.imena.ua
Updated Date: 2017-11-27T15:55:46Z
Creation Date: 2016-06-27T14:12:18Z
Registry Expiry Date: 2018-06-27T14:12:18Z
Registrar: INTERNET INVEST, LTD. DBA IMENA.UA
Registrar IANA ID: 1112
Registrar Abuse Contact Email: abuse@imena.ua
Registrar Abuse Contact Phone: +380.442010102
Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
Name Server: NSA3.SRV53.NET
Name Server: NSA3.SRV53.ORG
Name Server: NSA4.SRV53.COM
Name Server: NSB1.SRV53.NET
Name Server: NSB2.SRV53.COM
Name Server: NSB2.SRV53.ORG
Name Server: NSC4.SRV53.COM
Name Server: NSC4.SRV53.NET
Name Server: NSC4.SRV53.ORG
Name Server: NSD2.SRV53.NET
Name Server: NSD4.SRV53.COM
Name Server: NSD4.SRV53.ORG
DNSSEC: unsigned
URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of whois database: 2017-12-20T11:39:56Z <<<
```

## smcheck.org
Also in the script, but unused.  Currently dead.

### DiG
```
; <<>> DiG 9.10.3-P4-Ubuntu <<>> smcheck.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41318
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;smcheck.org.                   IN      A

;; AUTHORITY SECTION:
smcheck.org.            299     IN      SOA     ns-cloud-b1.googledomains.com. cloud-dns-hostmaster.google.com. 2 21600 3600 259200 300

;; Query time: 40 msec
;; SERVER: 127.0.1.1#53(127.0.1.1)
;; WHEN: Wed Dec 20 03:46:49 PST 2017
;; MSG SIZE  rcvd: 133
```

### WHOIS
```
Domain Name: SMCHECK.ORG
Registry Domain ID: D402200000004393769-LROR
Registrar WHOIS Server: whois.eurodns.com
Registrar URL: https://www.eurodns.com
Updated Date: 2017-12-04T11:53:35Z
Creation Date: 2017-12-03T13:33:40Z
Registry Expiry Date: 2019-12-03T13:33:40Z
Registrar Registration Expiration Date:
Registrar: EuroDNS S.A.
Registrar IANA ID: 1052
Registrar Abuse Contact Email: legalservices@eurodns.com
Registrar Abuse Contact Phone: +352.27220150
Reseller:
Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
Domain Status: serverTransferProhibited https://icann.org/epp#serverTransferProhibited
Registry Registrant ID: C199875880-LROR
Registrant Name: Whois Privacy
Registrant Organization: Whois Privacy (enumDNS dba)
Registrant Street: BPM 333868
Registrant Street: ZI Scheleck II 278
Registrant City: Bettembourg
Registrant State/Province:
Registrant Postal Code: 3225
Registrant Country: LU
Registrant Phone: +352.27720304
Registrant Phone Ext:
Registrant Fax:
Registrant Fax Ext:
Registrant Email: c039296ae86625d5_o@whoisprivacy.com
Registry Admin ID: C199875881-LROR
Admin Name: Whois Privacy
Admin Organization: Whois Privacy (enumDNS dba)
Admin Street: BPM 333868
Admin Street: ZI Scheleck II 278
Admin City: Bettembourg
Admin State/Province:
Admin Postal Code: 3225
Admin Country: LU
Admin Phone: +352.27720304
Admin Phone Ext:
Admin Fax:
Admin Fax Ext:
Admin Email: c039296ae86625d5_a@whoisprivacy.com
Registry Tech ID: C199903539-LROR
Tech Name: Whois Privacy
Tech Organization: Whois Privacy (enumDNS dba)
Tech Street: BPM 333868
Tech Street: ZI Scheleck II 278
Tech City: Bettembourg
Tech State/Province:
Tech Postal Code: 3225
Tech Country: LU
Tech Phone: +352.27720304
Tech Phone Ext:
Tech Fax:
Tech Fax Ext:
Tech Email: c039296ae86625d5_t@whoisprivacy.com
Name Server: NS-CLOUD-B1.GOOGLEDOMAINS.COM
Name Server: NS-CLOUD-B2.GOOGLEDOMAINS.COM
Name Server: NS-CLOUD-B3.GOOGLEDOMAINS.COM
Name Server: NS-CLOUD-B4.GOOGLEDOMAINS.COM
DNSSEC: unsigned
URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of WHOIS database: 2017-12-20T11:40:47Z <<<
```

None of the hostnames provide an index.


# Overview

1. The loader script is the first to load.
2. It sends a couple of POST requests to the /z endpoint on the ad network's domain, containing the following information (encrypted):
 * rf: window.location.href (URL the loader executed in, such as https://boards.4chan.org/vg)
 * fc: Whether the argon runtime has actually run, signalled by the "argon_enable=1" cookie.
 * fail: a list of any failures.
 * pw: Blank string.
3. The server returns an encrypted string.
4. The string decodes to a JSON object:
 * **pr**: Protocol, such as https.
 * **hs**: Hostname, such as piguiqproxy.com
 * **sb**: Subdomain, which is random. Example: r99d2
 * **ur**: Enormous random string, serving as the query part of the URL used next.
 * **pw:**: Password? Example: `4e9ce2feacc045a3a267e5ad50c55ea1`
 * **nn:**: Subdomain prefix. Example: `n4-`
5. The script then makes a GET to a URL with the format: `{pr}//{nn}{sb}.{hs}/{ur}`.  In the case of the examples above: `https://n4-r99d2.piguiqproxy.com/f1e4MWMFVlVVBgAFAgBWVQFTW`...
6. The server returns an encrypted javascript file with an encrypted section. This is the payload.
7. The loader script decrypts this section using a bitshifting obfuscation scheme, and a series of offsets hardcoded into the body of the loader itself.
8. The loader then executes the payload using `new Function('msz_'+gibberish, code)(B)`, referencing itself in `B`.


# Malicious Behaviours

Maliciousness of this system is KNOWN.

* Upon receiving a specific JSON message from the server, the loader script will activate a function (`t()`) that:
  * Disables all CSS stylesheets.
  * Removes the src attribute from all images.
* I have also noticed a `window.stop()`, which will break a *lot* of shit when invoked.

Maliciousness of downloaded payloads is UNKNOWN. (I haven't gotten to them yet.)

# Workarounds

1. Use NoScript and block:
 * 4chan.org (Shitty but needed to stop the embedded loader)
 * smcheck.org
 * piguiqproxy.com
 * amgload.net
2. Use the following UBlock Origin filters:

```
 # Recommended by /g/, adopted by UBlock already
 4chan.org##script:inject(abort-current-inline-script.js, String.fromCharCode)

 # Currently more accurate, but will break if the script tag moves. REMOVE THIS WHEN HIRO PULLS HIS HEAD FROM HIS ASS.
 4chan.org##:xpath(/html/head/script[6])
```

# Contact

* #4chan on irc.rizon.net as N3X15 (/query me to leave a message)
